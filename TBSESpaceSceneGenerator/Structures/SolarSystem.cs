﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBSESpaceSceneGenerator.Structures
{
    class SolarSystem
    {
        private static int Count = 0;

        public int ID { get; private set; }
        public Star Star { get; set; }
        public AsteroidBelt AsteroidBelt { get; set; }
        public List<Planet> Planets { get; private set; }
        public List<Comet> Comets { get; private set; }

        public SolarSystem()
        {
            this.ID = Count;

            Star = null;
            AsteroidBelt = null;
            Planets = new List<Planet>();
            Comets = new List<Comet>();

            Count++;
        }

        public static void ResetCount()
        {
            Count = 0;
        }

        public void AddPlanet(Planet planet)
        {
            Planets.Add(planet);
        }

        public void AddPlanets(List<Planet> planets)
        {
            Planets.AddRange(planets);
        }

        public void AddComet(Comet comet)
        {
            Comets.Add(comet);
        }

        public void AddComets(List<Comet> comets)
        {
            Comets.AddRange(comets);
        }
    }
}
